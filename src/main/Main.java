package main;

import libraries.Input;

/**
 * @author: Hannes Grall
 * @date: 10.12.2018
 **/
public class Main {

    public static void main(String[] args) {
        int command;
        boolean exit = false;
        Calendar myCalendar = new Calendar();

        while (!exit) { // same as exit == false
            System.out.println(
                    "1) Dates View\n" +
                    "2) Add Date\n" +
                    "3) Delete Date\n" +
                    "0) Exit\n");
            System.out.println("Please enter a command (0 to 3):\n");

            command = Input.readInt();

            switch (command) {
                case 1: {
                    myCalendar.showAppointments();
                    break;
                }
                case 2: {
                    myCalendar.addAppointment();
                    break;
                }
                case 3:
                    myCalendar.deleteAppointment();
                    break;
                case 0:
                    System.out.println("Command: Exit\n");
                    exit = true;
                    break;
            }
        }
    }
}