package main;

/**
 * @author: Hannes Grall
 * @date: 10.12.2018
 **/
public class DayListExt {
    private Day firstDayNode;

    // add appointment
    public void addAppointment(int day, Appointment appointment) {
        Day cur, prev;
        cur = firstDayNode;
        prev = null;

        // used for order
        while (cur != null && cur.getDay() < day) {
            prev = cur;
            cur = cur.getNext();
        }

        // node has to be created
        Day node = getDay(day); // look if there is already a day with appointments we can use
        if (node == null) { // if not we create a new object(node) ob the class Day
            node = new Day(day);
            node.addAppointment(appointment);
            node.setNext(cur);

            if (cur == firstDayNode) { // insert node as first element
                firstDayNode = node;
            } else {
                prev.setNext(node);
            }
        } else { // if a day(node) with appointments is already existing we use it
            node.addAppointment(appointment);
        }
    }

    // remove appointment
    public void removeAppointment(int day, Appointment appointment) {
        Day selected = getDay(day);
        selected.getAppointmentList().removeAppointment(appointment.getName()); //.removeIf(appoin -> appoin.getName().equals(appointment.getName()));

        if (selected.getAppointmentList().getSize() == 0) { // if the has no appointments anymore delete the day from the single linked list
            if (firstDayNode.getDay() == day) { // if firstDayNode has no appointments left, remove it from single linked list with setting second node to the firstDayNode
                firstDayNode = firstDayNode.getNext();
            } else { // else remove it with linking the day before to the day after
                Day cur = firstDayNode;
                while (cur.getNext().getDay() != day) {
                    cur = cur.getNext();
                }
                cur.setNext(cur.getNext().getNext());
            }
        }
    }

    // output of all appointments
    public void printList() {
        Day cur = firstDayNode;

        while (cur != null) { // if the variable cur is null the last day has been done and the output is complete
            System.out.println("Appointments on day " + cur.getDay());

            cur.getAppointmentList().printList(); // iterate over the appointments of a single day

            System.out.println("\n");
            cur = cur.getNext(); // fetch next day
        }
    }

    // helper method
    private Day getDay(int nr) {
        Day cur = firstDayNode;

        // stop iterating when cur.getDay() is the day we want to return
        while (cur != null && cur.getDay() != nr) {
            cur = cur.getNext(); // fetch next day (set the next day to the variable cur)
        }

        return cur;
    }
}