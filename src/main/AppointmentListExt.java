package main;

/**
 * @author: Hannes Grall
 * @date: 11.12.2018
 **/
public class AppointmentListExt {
    private Appointment firstAppointment;

    public void addAppointment(Appointment newNode) {
        Appointment cur, prev;
        cur = firstAppointment;
        prev = null;

        while (cur != null) {
            prev = cur;
            cur = cur.getNext();
        }

        newNode.setNext(null);

        if (firstAppointment == null) //insert node as first element
            firstAppointment = newNode;
        else
            prev.setNext(newNode);

    }

    public void removeAppointment(String name) {
        if (firstAppointment.getName().equals(name)) {
            firstAppointment = firstAppointment.getNext();
        } else {
            Appointment cur = firstAppointment;
            while (!cur.getNext().getName().equals(name)) { // same as (cur.getNext().getName().equals(name) == false)
                cur = cur.getNext();
            }
            cur.setNext(cur.getNext().getNext());
        }
    }

    public int getSize() {
        int counter = 0;
        Appointment cur = firstAppointment;

        while (cur != null) {
            counter++;
            cur = cur.getNext();
        }

        return counter;
    }

    public void printList() {
        Appointment cur = firstAppointment;

        while (cur != null) {
            System.out.println("Name: " + cur.getName());
            cur = cur.getNext();
        }
    }
}
