package main;

/**
 * @author: Hannes Grall
 * @date: 10.12.2018
 **/
public class Appointment {
    private String name;
    private Appointment next;

    public Appointment(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Appointment getNext() {
        return next;
    }

    public void setNext(Appointment next) {
        this.next = next;
    }
}