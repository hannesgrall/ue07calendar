package main;

import libraries.Input;

/**
 * @author: Hannes Grall
 * @date: 10.12.2018
 **/
public class Calendar {
    private DayListExt singleLinkedList;

    public Calendar() {
        singleLinkedList = new DayListExt();
    }

    public void addAppointment() {
        // selected command
        System.out.println("Command: Add Date");

        // user input
        System.out.println("Please enter the day of the appointment:");
        int appointmentDay = Input.readInt();
        System.out.println("Please enter a name for this appointment:");
        String appointmentName = Input.readString();

        // add
        singleLinkedList.addAppointment(appointmentDay, new Appointment(appointmentName));
    }

    public void deleteAppointment() {
        // selected command
        System.out.println("Command: Delete Date\n");

        // user input
        System.out.println("Please enter the day of the appointment which should be deleted:\n");
        int toDeleteAppointmentDay = Input.readInt();
        System.out.println("Please enter a name for the appointment which should be deleted:\n");
        String toDeleteAppointmentName = Input.readString();

        // delete
        singleLinkedList.removeAppointment(toDeleteAppointmentDay, new Appointment(toDeleteAppointmentName));
    }

    public void showAppointments() {
        // selected command
        System.out.println("Command: Dates View\n");

        // console output of appointment list
        singleLinkedList.printList();
    }
}