package main;

/**
 * @author: Hannes Grall
 * @date: 10.12.2018
 **/
public class Day {
    private int day;
    private AppointmentListExt appointmentList;
    private Day next;

    public Day(int day) {
        checkValidityOfDay(day);
        this.day = day;
        this.appointmentList = new AppointmentListExt();
    }

    public int getDay() {
        return day;
    }

    public Day getNext() {
        return next;
    }

    public void setNext(Day next) {
        this.next = next;
    }

    public AppointmentListExt getAppointmentList() {
        return appointmentList;
    }

    public void addAppointment(Appointment appointment) {
        appointmentList.addAppointment(appointment);
    }

    private void checkValidityOfDay(int day) {
        if (day < 1 || day > 31) {
            throw new IllegalArgumentException("Entered day has to be between 1 and 31.");
        }
    }
}